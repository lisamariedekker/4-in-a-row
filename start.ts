import * as readline from 'readline'

async function main() {

  const board = [
    ["-", "-", "-", "-", "-", "-", "-"],
    ["-", "-", "-", "-", "-", "-", "-"],
    ["-", "-", "-", "-", "-", "-", "-"],
    ["-", "-", "-", "-", "-", "-", "-"],
    ["-", "-", "-", "-", "-", "-", "-"],
    ["-", "-", "-", "-", "-", "-", "-"],
  ]

  let turn = 0

  while (true) {
    const ans = await askQuestion("Which column number? ")

    const number = parseInt(ans)
    if (number <= 0 || number >= 8 || isNaN(number)) console.log("invalid input, please enter a number between 1 and 7")

    const x = number - 1

    if (!insert(board, x, turn)) {
      console.log("Cannot insert, full")
    } else {
      turn++
      const winner = checkWinner(board)
      if (winner) {
        console.log(`Player ${turn%2 ? "o" : "x"} is the winner!`)
        console.log(board)
        return
      }

      console.log("Your answer:", number)
      console.log(board)
      console.log(turn)
    }
  }
}

function checkLine(a, b, c, d) {
  return ((a != "-") && (a == b) && (a == c) && (a == d))
}

function checkWinner(board: string[][]) {
  // Check down
  for (let r = 0;r < 3;r++)
    for (let c = 0;c < 7;c++)
      if (checkLine(board[r][c], board[r + 1][c], board[r + 2][c], board[r + 3][c]))
        return board[r][c]

  // Check right
  for (let r = 0;r < 6;r++)
    for (let c = 0;c < 4;c++)
      if (checkLine(board[r][c], board[r][c + 1], board[r][c + 2], board[r][c + 3]))
        return board[r][c]

  // Check down-right
  for (let r = 0;r < 3;r++)
    for (let c = 0;c < 4;c++)
      if (checkLine(board[r][c], board[r + 1][c + 1], board[r + 2][c + 2], board[r + 3][c + 3]))
        return board[r][c]

  // Check down-left
  for (let r = 3;r < 6;r++)
    for (let c = 0;c < 4;c++)
      if (checkLine(board[r][c], board[r - 1][c + 1], board[r - 2][c + 2], board[r - 3][c + 3]))
        return board[r][c]

  return 0
}

function insert(board: string[][], x: number, turn: number) {
  for (let y = 5;y >= 0;y--) {
    const value = board[y][x]
    if (value === "-") {
      (turn % 2 === 0 ? board[y][x] = "o" : board[y][x] = "x")
      return true
    }
  }
  return false
}

// function isFull(board: string[][], x: number){

//   for(let y = 5; y >= 0; y--){
//     const value = board[y][x]
//     if (value === "-") return false
//   }

//   return true
// }

function askQuestion(query: string) {

  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  })

  return new Promise<string>(resolve => rl.question(query, ans => {
    rl.close()
    resolve(ans)
  }))
}

main().catch(console.error)